﻿Shader "VFX/DegradableUISurface"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Main Color", Color) = (1,.5,.5,1)
		_Degradation ("Degradation level", Range(0,1)) = 0.0
		_DegradationEvolutionRatio ("Degradation propagation range", Range(0,1)) = 0.1
		_AltText ("Degrading Texture (RGB)", 2D) = "white" {}
		_AltHeightmap ("Degrading Texture Heightmap", 2D) = "white" {}

	}
	SubShader
	{
		Tags { "Queue" = "Transparent" }
        Blend SrcAlpha OneMinusSrcAlpha
		ZTest Always Cull Off ZWrite Off

		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				half4 color : COLOR0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				half4 vc : COLOR0;
			};

			sampler2D _MainTex;
			half4 _Color;
			float4 _MainTex_ST;
			half _Degradation;
			half _DegradationEvolutionRatio;
			sampler2D _AltText;
			sampler2D _AltHeightmap;
			half degradDt;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.vc = v.color * _Color;
				return o;
			}
			
			half4 frag (v2f i) : COLOR
			{
				// Albedo comes from a texture tinted by color
				fixed4 c = tex2D (_MainTex, i.uv) * i.vc;
				// Add degradation by level
				fixed4 degPx = tex2D (_AltText, i.uv);
				// Get degradation height
				fixed4 degHeight = tex2D (_AltHeightmap, i.uv);
			
				degradDt = degHeight.r - _Degradation;
			
				if (degradDt > 0)
				{
					if (degradDt > _DegradationEvolutionRatio) {
						c.rgb = degPx.rgb;
					} else {
						degradDt = degradDt / _DegradationEvolutionRatio;
						c.rgb = (c.rgb * (1.0 - degradDt)) + (degPx.rgb * degradDt);
					}
				}
	
				return c;
			}
			ENDCG
		}
	}
}
