Shader "VFX/PostProcessing/EditableColorEffect" {
Properties {
	_MainTex ("Base (RGB)", 2D) = "white" {}
	_Intensity ("Intensity (RGB)", Vector) = (0.299, 0.587, 0.114)
	_ColorConvert ("Color Conversion (RGBA)", Vector) = (0.191, -0.054, -0.221, 0.0)
}

SubShader {
	Pass {
		ZTest Always Cull Off ZWrite Off
				
CGPROGRAM
#pragma vertex vert_img
#pragma fragment frag
#include "UnityCG.cginc"

uniform sampler2D _MainTex;
uniform fixed3 _Intensity;
uniform fixed4 _ColorConvert;

fixed4 frag (v2f_img i) : SV_Target
{	
	fixed4 original = tex2D(_MainTex, i.uv);
	
	// get intensity value (Y part of YIQ color space)
	fixed Y = dot (fixed3(_Intensity.x, _Intensity.y, _Intensity.z), original.rgb);

	// Convert to Sepia Tone by adding constant
	fixed4 sepiaConvert = _ColorConvert;
	fixed4 output = sepiaConvert + Y;
	output.a = original.a;
	
	return output;
}
ENDCG

	}
}

Fallback off

}
