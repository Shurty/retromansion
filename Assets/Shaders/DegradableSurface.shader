﻿Shader "VFX/DegradableSurface" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
		_Degradation ("Degradation level", Range(0,1)) = 0.0
		_DegradationEvolutionRatio ("Degradation propagation range", Range(0,1)) = 0.1
		_AltText ("Degrading Texture (RGB)", 2D) = "white" {}
		_AltHeightmap ("Degrading Texture Heightmap", 2D) = "white" {}
	}
	SubShader {

		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM

		#pragma surface surf Standard fullforwardshadows
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
		};

		half _Glossiness;
		half _Metallic;
		half _Degradation;
		half _DegradationEvolutionRatio;
		fixed4 _Color;
		sampler2D _AltText;
		sampler2D _AltHeightmap;

		half degradDt;

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			// Add degradation by level
			fixed4 degPx = tex2D (_AltText, IN.uv_MainTex) * _Color;
			// Get degradation height
			fixed4 degHeight = tex2D (_AltHeightmap, IN.uv_MainTex);
			
			degradDt = degHeight.r - _Degradation;
			
			if (degradDt > 0)
			{
				if (degradDt > _DegradationEvolutionRatio) {
					c.rgb = degPx.rgb;
				} else {
					degradDt = degradDt / _DegradationEvolutionRatio;
					c.rgb = (c.rgb * (1.0 - degradDt)) + (degPx.rgb * degradDt);
				}
			}
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG

	} 
	FallBack "Diffuse"
}
