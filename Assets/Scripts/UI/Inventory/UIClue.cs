﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	using UnityEngine.UI;
	//using UnityEngine.Events;

	[AddComponentMenu("Scripts/UI/Clue")]
	//[RequireComponent(typeof())]

#endregion

///<summary>
/// 
///</summary>
public class UIClue : MonoBehaviour
{

	#region Members

		// References

		[Header("References")]

		[Tooltip("The object to set active/disabled when a clue is open/close.")]
		[SerializeField]	private		GameObject	m_UIClueMain	= null;

		[SerializeField]	private		Image		m_UIClueImage	= null;

	#endregion

	
	#region Initialisation / Destruction
	#endregion

	
	#region Actions

		public void OpenClue(Clue _Clue)
		{
			if(m_UIClueMain.activeSelf)
			{
				CloseClue();
				OpenClue(_Clue);
			}

			else
			{
				m_UIClueMain.SetActive(true);

				m_UIClueImage.sprite = _Clue.ClueImage;
			}
		}

		public void CloseClue()
		{
			m_UIClueMain.SetActive(false);
		}

	#endregion

	
	#region Events
	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors
	#endregion

}