﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	using UnityEngine.UI;
	//using UnityEngine.Events;

	[AddComponentMenu("Scripts/UI/Player Inventory")]
	//[RequireComponent(typeof())]

#endregion

///<summary>
/// 
///</summary>
public class UIPlayerInventory : MonoSingleton<UIPlayerInventory>
{

	#region Members

		// Constants

							private		const	float					c_InventoryItemPadding	= 5.0f;
							private		const	float					c_InventoryItemHeight	= 60.0f;

		// Resources

		[Header("Inventory resources")]

		[SerializeField]	private				UIInventoryItem			m_UIInventoryItemPrefab	= null;

		// References

			// Inventory

		[Header("Inventory references")]

		[Tooltip("The object to set active/disabled when the Inventory is open/closed.")]
		[SerializeField]	private				GameObject				m_UIInventoryMain		= null;

		[SerializeField]	private				Transform				m_InventoryItemsParent	= null;

			// Clues

		[Header("Clues references")]

		[Tooltip("The object to set active/disabled when a clue is open/closed.")]
		[SerializeField]	private				GameObject				m_UIClueMain		= null;

		[SerializeField]	private				Image					m_UIClueImage		= null;
		
	#endregion

	
	#region Initialisation / Destruction
	#endregion

	
	#region Actions

		public void ToggleInventory()
		{
			if (m_UIInventoryMain.activeSelf)
			{
				CloseInventory();
			}

			else
			{
				OpenInventory();
			}
		}

		private void OpenInventory()
		{
			if (!UIPlayerInventory.Instance.m_UIInventoryMain.activeSelf)
			{
				UIPlayerInventory.Instance.m_UIInventoryMain.SetActive(true);
				UIPlayerInventory.Instance.m_UIClueMain.SetActive(false);

				GenerateUIInventoryItems();

				GameManager.Instance.PlayerInstance.OnInventoryOpen();
			}
		}

		private void CloseInventory()
		{
			DestroyInventoryItems();

			m_UIInventoryMain.SetActive(false);

			GameManager.Instance.PlayerInstance.OnInventoryClosed();
		}

		public void OpenClue(Clue _Clue)
		{
			OpenInventory();

			UIPlayerInventory.Instance.m_UIClueMain.SetActive(true);

			UIPlayerInventory.Instance.m_UIClueImage.sprite = _Clue.ClueImage;
		}

		public void CloseClue()
		{
			m_UIClueMain.SetActive(false);
		}

		/// <summary>
		/// Generate UI for inventory items.
		/// </summary>
		private void GenerateUIInventoryItems()
		{
			List<PickableElement> inventoryItems = PlayerInventory.Instance.PickableElements;

			Vector2 anchoredPos = new Vector2(0.0f, -(c_InventoryItemHeight / 2 + c_InventoryItemPadding));

			for(int i = 0; i < inventoryItems.Count; i++)
			{
				GameObject newObj = Instantiate(m_UIInventoryItemPrefab.gameObject, InventoryItemsParent.position, Quaternion.identity) as GameObject;

				newObj.transform.SetParent(InventoryItemsParent, false);

				PlaceUIInventoryItem(newObj.GetComponent<RectTransform>(), anchoredPos);
				InitInventoryItem(newObj.GetComponent<UIInventoryItem>(), inventoryItems[i]);

				anchoredPos.y -= c_InventoryItemHeight + c_InventoryItemPadding;
			}

			RectTransform rectTr = InventoryItemsParent.GetComponent<RectTransform>();

			rectTr.sizeDelta	= new Vector2(0.0f, c_InventoryItemPadding + (c_InventoryItemHeight+ c_InventoryItemPadding) * inventoryItems.Count);
			
			Vector3 rectTrPosition = rectTr.position;

			rectTrPosition.y = 0.0f;
			rectTr.position = rectTrPosition;
		}

		/// <summary>
		/// Place an InventoryItem on its Canvas.
		/// </summary>
		private void PlaceUIInventoryItem(RectTransform _RectTransform, Vector2 _AnchoredPosition)
		{
			_RectTransform.anchoredPosition = _AnchoredPosition;

			_AnchoredPosition.y -= c_InventoryItemHeight + c_InventoryItemPadding;
		}

		private void InitInventoryItem(UIInventoryItem _UIInventoryItem, PickableElement _Pickable)
		{
			_UIInventoryItem.SetInventoryItem(_Pickable);
		}

		private void DestroyInventoryItems()
		{
			UIInventoryItem[] items = InventoryItemsParent.GetComponentsInChildren<UIInventoryItem>();

			for(int i = 0; i < items.Length; i++)
			{
				Destroy(items[i].gameObject);
			}
		}

	#endregion

	
	#region Events
	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors

		private Transform InventoryItemsParent
		{
			get { return (UIPlayerInventory.Instance.m_InventoryItemsParent) ? UIPlayerInventory.Instance.m_InventoryItemsParent : transform; }
		}

	#endregion

}