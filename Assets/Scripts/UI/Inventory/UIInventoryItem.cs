﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	using UnityEngine.UI;
	//using UnityEngine.Events;

	[AddComponentMenu("Scripts/UI/Inventory Item")]
	//[RequireComponent(typeof())]

#endregion

///<summary>
/// 
///</summary>
public class UIInventoryItem : MonoBehaviour
{

	#region Members

		// References

		[Header("References")]

		[SerializeField]	private		Image	m_InventoryItemIconField	= null;
		[SerializeField]	private		Text	m_InventoryItemNameField	= null;

		// Flow

							private		string	m_InventoryItemName			= string.Empty;

	#endregion

	
	#region Initialisation / Destruction
	#endregion

	
	#region Actions

		public void UseItem()
		{
			PlayerInventory.Instance.UseElement(m_InventoryItemName);
		}

	#endregion

	
	#region Events
	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors

		public void SetInventoryItem(PickableElement _PickableElement)
		{
			m_InventoryItemIconField.sprite = _PickableElement.UIItemView;
			m_InventoryItemNameField.text	= _PickableElement.ItemName;

			m_InventoryItemName				= _PickableElement.ItemName;
		}

	#endregion

}