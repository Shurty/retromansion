﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum UIActionTarget
{
    NONE,
    PLAY,
    CREDITS,
    MAINMENU,
    HELP
};

public class UIMainMenuController : MonoBehaviour {

    public UIScribbleSprite[] m_scribbles;
    public float nextScribbleUpdate = 0.0f;
    public float scribbleUpdateDelay = 0.1f;
    public Image fadingBlackOverlay;
    public bool uiLocked = false;
    public UIActionTarget uiTargetAction;

	// Use this for initialization
	void Start () {
        fadingBlackOverlay.gameObject.SetActive(true);
        fadingBlackOverlay.color = new Color(0, 0, 0, 1.0f);
        uiLocked = false;
    }
	
	// Update is called once per frame
	void Update () {
        nextScribbleUpdate -= Time.deltaTime;

        if (nextScribbleUpdate < 0)
        {
            UpdateScribbles();
            nextScribbleUpdate = scribbleUpdateDelay;
        }

        float activeAlpha = fadingBlackOverlay.color.a - Time.deltaTime;

        if (uiLocked)
        {
            activeAlpha = fadingBlackOverlay.color.a + Time.deltaTime;
            if (activeAlpha >= 1.0f)
            {
                switch (uiTargetAction)
                {
                    case UIActionTarget.PLAY:
                        Application.LoadLevel("HallRDC");
                        break;
                    case UIActionTarget.CREDITS:
                        Application.LoadLevel("Credits");
                        break;
                    case UIActionTarget.HELP:
                        Application.LoadLevel("Help");
                        break;
                    default:
                        break;
                }
                uiLocked = false;
                //activeAlpha = 0.0f;
            }
        }
        fadingBlackOverlay.color = new Color(0, 0, 0, Mathf.Clamp(activeAlpha, 0.0f, 1.0f));
    }

    void UpdateScribbles()
    {
        for (int i = 0; i < m_scribbles.Length; ++i)
        {
            m_scribbles[i].activeScribble++;
            if (m_scribbles[i].activeScribble >= m_scribbles[i].scribbles.Length)
                m_scribbles[i].activeScribble = 0;

            int act = m_scribbles[i].activeScribble;
            m_scribbles[i].scribbleInstance.sprite = m_scribbles[i].scribbles[act];
        }
    }

    public void OnPlayBtnPressed()
    {
        if (uiLocked)
            return;
        uiLocked = true;
        uiTargetAction = UIActionTarget.PLAY;
    }

    public void OnHelpBtnPressed()
    {
        if (uiLocked)
            return;
        uiLocked = true;
        uiTargetAction = UIActionTarget.HELP;
    }

    public void OnCreditsBtnPressed()
    {
        if (uiLocked)
            return;
        uiLocked = true;
        uiTargetAction = UIActionTarget.CREDITS;
    }

    public void OnExitBtnPressed()
    {
        if (uiLocked)
            return;

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
	    Application.Quit();
#endif
    }

}
