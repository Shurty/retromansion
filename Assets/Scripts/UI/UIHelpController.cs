﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIHelpController : MonoBehaviour {

    public UIScribbleSprite[] m_scribbles;
    public float nextScribbleUpdate = 0.0f;
    public float scribbleUpdateDelay = 0.1f;
    public Image fadingBlackOverlay;
    public bool uiLocked = false;
    public UIActionTarget uiTargetAction;

	// Use this for initialization
	void Start () {
        fadingBlackOverlay.gameObject.SetActive(true);
        fadingBlackOverlay.color = new Color(0, 0, 0, 1.0f);
        uiLocked = false;
	}
	
	// Update is called once per frame
	void Update () {
        nextScribbleUpdate -= Time.deltaTime;

        if (nextScribbleUpdate < 0)
        {
            UpdateScribbles();
            nextScribbleUpdate = scribbleUpdateDelay;
        }

        float activeAlpha = fadingBlackOverlay.color.a - Time.deltaTime;

        if (uiLocked)
        {
            activeAlpha = fadingBlackOverlay.color.a + Time.deltaTime;
            if (activeAlpha >= 1.0f)
            {
                switch (uiTargetAction)
                {
                    case UIActionTarget.MAINMENU:
                        Application.LoadLevel("MainMenu");
                        break;
                    default:
                        break;
                }
                uiLocked = false;
                //activeAlpha = 0.0f;
            }
        }
        fadingBlackOverlay.color = new Color(0, 0, 0, Mathf.Clamp(activeAlpha, 0.0f, 1.0f));
    }

    void UpdateScribbles()
    {
        for (int i = 0; i < m_scribbles.Length; ++i)
        {
            m_scribbles[i].activeScribble++;
            if (m_scribbles[i].activeScribble >= m_scribbles[i].scribbles.Length)
                m_scribbles[i].activeScribble = 0;

            int act = m_scribbles[i].activeScribble;
            m_scribbles[i].scribbleInstance.sprite = m_scribbles[i].scribbles[act];
        }
    }

    public void OnBackBtnPressed()
    {
        if (uiLocked)
            return;
        uiLocked = true;
        uiTargetAction = UIActionTarget.MAINMENU;
    }
}
