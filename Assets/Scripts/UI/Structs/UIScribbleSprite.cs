﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[System.Serializable]
public struct UIScribbleSprite {

    public Image textInstance;
    public Image scribbleInstance;

    public Sprite[] scribbles;
    public int activeScribble;

}
