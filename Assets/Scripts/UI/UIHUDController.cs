﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public enum UIRoomNamesRelations
{
    ROOM_BUANDRIE,
    ROOM_CUISINE,
    ROOM_HALL,
    ROOM_SALLEAMANGER,
    ROOM_SALON,
    ROOM_VERANDA,
}

public class UIHUDController : MonoBehaviour {

    public UIScribbleSprite[] m_scribbles;
    public Sprite[] m_texts;
    public UIScribbleSprite m_activeScribble;
    public float m_nextScribbleUpdate = 0.0f;
    public float m_scribbleUpdateDelay = 0.1f;
    public Image m_fadingBlackOverlay;
    public bool m_uiLocked = false;
    public UIActionTarget m_uiTargetAction;

    public GameObject m_pausePanel;

    public Image m_roomNameOverlay;
    public Image m_roomNameDetailsOverlay;
    public float m_roomFadeoutDelay = 2.0f;
    public int m_room;

	// Use this for initialization
	void Start () {
        m_fadingBlackOverlay.gameObject.SetActive(true);
        m_pausePanel.SetActive(false);
        m_fadingBlackOverlay.color = new Color(0, 0, 0, 1.0f);
        m_uiLocked = false;
        SetActiveRoom((UIRoomNamesRelations)m_room);
	}
	
    public void SetActiveRoom(UIRoomNamesRelations uiId)
    {
        m_roomNameOverlay.sprite = m_texts[(int)uiId];
        m_activeScribble = m_scribbles[(int)uiId];
    }

	// Update is called once per frame
	void Update () {
        m_nextScribbleUpdate -= Time.deltaTime;
        if (m_nextScribbleUpdate < 0)
        {
            UpdateScribbles();
            m_nextScribbleUpdate = m_scribbleUpdateDelay;
        }

        float activeAlpha = m_fadingBlackOverlay.color.a - Time.deltaTime;

        if (m_uiLocked)
        {
            activeAlpha = m_fadingBlackOverlay.color.a + Time.deltaTime;
            if (activeAlpha >= 1.0f)
            {
                switch (m_uiTargetAction)
                {
                    case UIActionTarget.MAINMENU:
                        Application.LoadLevel("MainMenu");
                        break;
                    default:
                        break;
                }
                m_uiLocked = false;
                //activeAlpha = 0.0f;
            }
        }
        m_fadingBlackOverlay.color = new Color(0, 0, 0, Mathf.Clamp(activeAlpha, 0.0f, 1.0f));
        
        // Apply room name effect
        m_roomFadeoutDelay = Mathf.Clamp(m_roomFadeoutDelay - Time.deltaTime, 0.0f, m_roomFadeoutDelay);
        m_roomNameOverlay.color = new Color(1.0f, 1.0f, 1.0f, Mathf.Clamp(m_roomFadeoutDelay, 0.0f, 1.0f));
        m_roomNameDetailsOverlay.color = m_roomNameOverlay.color;
    }

    void UpdateScribbles()
    {
        for (int i = 0; i < m_scribbles.Length; ++i)
        {
            m_scribbles[i].activeScribble++;
            if (m_scribbles[i].activeScribble >= m_scribbles[i].scribbles.Length)
                m_scribbles[i].activeScribble = 0;

            int act = m_scribbles[i].activeScribble;
            m_scribbles[i].scribbleInstance.sprite = m_scribbles[i].scribbles[act];
        }
    }

    void ShowPause()
    {
        m_pausePanel.SetActive(true);
    }

    void HidePause()
    {
        m_pausePanel.SetActive(false);
    }

    public void OnPauseBtnPressed()
    {
        ShowPause();
    }

    public void OnResumeBtnPressed()
    {
        if (m_uiLocked)
            return;
        HidePause();
    }

    public void OnMainMenuBtnPressed()
    {
        if (m_uiLocked)
            return ;
        m_uiLocked = true;
        m_uiTargetAction = UIActionTarget.MAINMENU;
    }
}
