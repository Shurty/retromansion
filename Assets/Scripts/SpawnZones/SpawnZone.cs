﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class SpawnZone : MonoBehaviour
{
    
	[Header("Assets to spawn")]

    [SerializeField]
    List<GameObject> m_SpawnItemsList = new List<GameObject>();

    [SerializeField]
    MeshesDegradationController m_DegradationController = null;

    void Start()
    {
        SpawnAsset();
    }

	protected abstract void SpawnAsset();

    protected Vector3 GetBigestAssetSize()
    {
        Vector3 size = Vector3.zero;
        Vector3 currentSize = Vector3.zero;

        for(int i = 0; i < m_SpawnItemsList.Count; i++)
        {
            if (m_SpawnItemsList[i].GetComponent<MeshRenderer>())
            {
                currentSize = m_SpawnItemsList[i].GetComponent<MeshRenderer>().bounds.size;
            }
            else
            {
                foreach (MeshRenderer rend in m_SpawnItemsList[i].GetComponentsInChildren<MeshRenderer>())
                {
                    currentSize = rend.bounds.size;
                    break;
                }
            }

            size.x = (currentSize.x > size.x) ? currentSize.x : size.x;
            size.y = (currentSize.y > size.y) ? currentSize.y : size.y;
            size.z = (currentSize.z > size.z) ? currentSize.z : size.z;
        }

        return size;
    }

	protected Vector3 GetPivot()
	{
		Vector3 pivot = transform.position;

		MeshRenderer firstRenderer = (m_SpawnItemsList.Count > 0) ? m_SpawnItemsList[0].GetComponent<MeshRenderer>() : null;

		if(firstRenderer != null)
		{
			pivot = firstRenderer.bounds.center;
		}

		return transform.position + pivot;
	}

    protected GameObject RandomAsset
    {
        get
        {
            if (m_SpawnItemsList.Count > 0)
            {
                int randomIndex = Random.Range(0, m_SpawnItemsList.Count);

                return m_SpawnItemsList[randomIndex];
            }
            else
            {
                return null;
            }
        }
    }

    protected void PushForDegradationController(GameObject obj)
    {
        if (m_DegradationController == null)
            return;
        if (obj.GetComponent<MeshRenderer>())
        {
            m_DegradationController.m_targetMeshes.Add(obj.GetComponent<MeshRenderer>());
        }
        else
        {
            foreach (MeshRenderer rend in obj.GetComponentsInChildren<MeshRenderer>())
            {
                m_DegradationController.m_targetMeshes.Add(rend);
            }
        }
    }

}
