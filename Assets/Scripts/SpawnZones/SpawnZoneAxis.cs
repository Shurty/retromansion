﻿using UnityEngine;
using System.Collections;

public class SpawnZoneAxis : SpawnZone
{
    [SerializeField]
    float m_SizeRoomX;
    [SerializeField]
    float m_SizeRoomZ;
    protected override void SpawnAsset()
    {
		GameObject assetToSpawn = RandomAsset;

		if (assetToSpawn != null)
		{
			Vector3 position = transform.position;
			float minX = position.x - m_SizeRoomX;
			float maxX = position.x + m_SizeRoomX;
			float minZ = position.z - m_SizeRoomZ;
			float maxZ = position.z + m_SizeRoomZ;
			position.x = Random.Range(minX, maxX);
			position.z = Random.Range(minZ, maxZ);
			
			Object item = Instantiate(assetToSpawn, position, transform.rotation);
            PushForDegradationController(item as GameObject);
        }

		else
		{
			Debug.LogWarning("The list is empty.");
		}
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        Vector3 center	= transform.position;

        Vector3 left	= -transform.right * m_SizeRoomX;
        Vector3 right	= transform.right * m_SizeRoomX;
        Vector3 forward	= transform.forward * m_SizeRoomZ;
        Vector3 back	= -transform.forward * m_SizeRoomZ;

        Vector3 size	= new Vector3(m_SizeRoomX*2,0, m_SizeRoomZ*2);

		Gizmos.DrawWireCube(center, size);

		Vector3 pivot	= GetPivot();

        Vector3 bigestAssetSize		= GetBigestAssetSize();

		Gizmos.DrawWireCube(pivot + right + forward, bigestAssetSize);
		Gizmos.DrawWireCube(pivot + left + forward, bigestAssetSize);
		Gizmos.DrawWireCube(pivot + right + back, bigestAssetSize);
		Gizmos.DrawWireCube(pivot + left + back, bigestAssetSize);
    }

}
