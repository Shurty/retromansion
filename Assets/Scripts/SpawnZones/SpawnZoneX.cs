﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpawnZoneX : SpawnZone
{

	[Header("Sizes")]

    [SerializeField]
    float m_SizeRoomX;

    protected override void SpawnAsset()
    {
        GameObject assetToSpawn = RandomAsset;

        if (assetToSpawn != null)
        {
            Vector3 position = transform.position;
            float minX = position.x - m_SizeRoomX;
            float maxX = position.x + m_SizeRoomX;
			position.x = Random.Range(minX, maxX);

            Object item = Instantiate(assetToSpawn, position, transform.rotation);
            PushForDegradationController(item as GameObject);
        }

        else
        {
            Debug.LogWarning("The list is empty.");
        }
    }
   
    private void OnDrawGizmos()
    {
        Gizmos.color		= Color.yellow;

        Vector3 position	= transform.position;

        Vector3 left		= -transform.right * m_SizeRoomX;
        Vector3 right		= transform.right * m_SizeRoomX;

        Gizmos.DrawLine(position, position+right);
        Gizmos.DrawLine(position, position+left);

        Vector3 bigestAssetSize = GetBigestAssetSize();

		Vector3 pivot = GetPivot();

		Gizmos.DrawWireCube(pivot + right, bigestAssetSize);
		Gizmos.DrawWireCube(pivot + left, bigestAssetSize);
    }

}
