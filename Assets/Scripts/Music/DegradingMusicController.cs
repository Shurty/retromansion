﻿using UnityEngine;
using System.Collections;

public class DegradingMusicController : MonoBehaviour {

    public DegradingMusicKey[] m_musicKeys;
    public AudioSource m_playedMusic;
    public AudioSource m_fadingInMusic;
    public float m_degressionRatio = 0.0f;

    public DegradingMusicKey m_activeMusicKey;
    public DegradingMusicKey m_nextMusicKey;

	// Use this for initialization
	void Start () {
	
	}
	
    void UpdateActiveMusicKey()
    {
        foreach (DegradingMusicKey mk in m_musicKeys)
        {
            if (mk.degradationLevel > m_degressionRatio)
                return;
            m_activeMusicKey = mk;
        }
    }

    void UpdateNextMusicKey()
    {
        foreach (DegradingMusicKey mk in m_musicKeys)
        {
            if (mk.degradationLevel > m_degressionRatio)
            {
                m_nextMusicKey = mk;
                return;
            }
        }
        m_nextMusicKey = m_activeMusicKey;
    }

    void UpdateSoundRatios()
    {
        float relativeDegr = m_degressionRatio - m_activeMusicKey.degradationLevel;
        float degDiff = m_nextMusicKey.degradationLevel - m_activeMusicKey.degradationLevel;
        float volRatio = relativeDegr / degDiff;
        float timeLoc = m_playedMusic.time;

        if (m_playedMusic.clip != m_activeMusicKey.music)
        {
            m_playedMusic.clip = m_activeMusicKey.music;
            m_playedMusic.Play();
            m_playedMusic.time = timeLoc;
        }
        if (m_fadingInMusic.clip != m_nextMusicKey.music)
        {
            m_fadingInMusic.clip = m_nextMusicKey.music;
            m_fadingInMusic.Play();
            m_fadingInMusic.time = timeLoc;
        }
        m_playedMusic.volume = (1.0f - volRatio);
        m_fadingInMusic.volume = volRatio;
        m_fadingInMusic.timeSamples = m_playedMusic.timeSamples;
    }

	// Update is called once per frame
	void Update () {
        UpdateActiveMusicKey();
        UpdateNextMusicKey();
        UpdateSoundRatios();
	}
}
