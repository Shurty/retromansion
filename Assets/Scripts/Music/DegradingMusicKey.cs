﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct DegradingMusicKey {

    public AudioClip music;
    public float degradationLevel;

}
