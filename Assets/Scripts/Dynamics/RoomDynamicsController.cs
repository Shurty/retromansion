﻿using UnityEngine;
using System.Collections;

public class RoomDynamicsController : MonoBehaviour {

    public ParticleSystem[] m_particleSystems;
    public Transform[] m_lightAnchors;
    public Vector3 m_activeWindVelocity;
    public float m_lightRotationRatio = 4.0f;

    // <summary>
    //    -
    // </summary>
	void Start () {
	
	}

    // <summary>
    //    Update particle system particles velocities
    // </summary>
    void UpdateParticleSystems()
    {
        foreach (ParticleSystem ps in m_particleSystems)
        {
            ParticleSystem.Particle[] particles = new ParticleSystem.Particle[ps.particleCount + 1];

            int pcount = ps.GetParticles(particles);
            for (int i = 0; i < pcount; ++i)
            {
                particles[i].velocity = m_activeWindVelocity;
            }
            ps.SetParticles(particles, pcount);
        }
    }

    // <summary>
    //    Update light system anchors velocities
    // </summary>
    void UpdateLightSystems()
    {
        foreach (Transform tr in m_lightAnchors)
        {
            tr.eulerAngles = new Vector3(-m_activeWindVelocity.z, 0, m_activeWindVelocity.x) * m_lightRotationRatio;
        }
    }

    // <summary>
    //    -
    // </summary>
	void Update () {
        UpdateParticleSystems();
        UpdateLightSystems();
    }
}
