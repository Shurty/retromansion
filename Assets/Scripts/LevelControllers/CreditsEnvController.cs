﻿using UnityEngine;
using System.Collections;

public class CreditsEnvController : MonoBehaviour {

    public MeshesDegradationController meshDegFilter;
    public float degLevel = 0.0f;
    public float degSpeed = 0.1f;
    public float degLevelDirection = 1.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        degLevel += Time.deltaTime * degSpeed * degLevelDirection;

        if (degLevel > 1.0f)
        {
            degLevel = 1.0f;
            degLevelDirection = -1.0f;
        }

        if (degLevel < 0.0f)
        {
            degLevel = 0.0f;
            degLevelDirection = 1.0f;
        }

        meshDegFilter.m_rustLevel = degLevel;
	}
}
