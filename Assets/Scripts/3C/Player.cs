﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[AddComponentMenu("Scripts/Player")]
	[RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(PlayerController))]
    [RequireComponent(typeof(Rigidbody))]

#endregion

///<summary>
/// The Player require a child object with a trigger to interact with interactable in scene
///</summary>
public class Player : MonoBehaviour
{

	#region Members
        
    private Camera                  m_MainCamera;
    private NavMeshAgent            m_NavMeshAgent;
    private InteractableElement     m_LastAimedInteractableElement      =   null;//the last interactable element the player clicked, and we are moving to
    private int                     m_MovementPoints                    =   0;
    private Animator                m_AnimSystem;

	#endregion
	
	#region Initialisation / Destruction

    /// <summary>
    /// Instanciate the players variable that needs to be instanciated only once, and set it to dontDestroyOnLoad
    /// Called only one time even when loading another scene
    /// </summary>
    private void Awake()
    {
        NavMeshAgent navAgent = GetComponentInChildren<NavMeshAgent>();
        m_AnimSystem = GetComponentInChildren<Animator>();
        if (navAgent != null)
        {
            m_NavMeshAgent = navAgent;
        }
        else
        {
            gameObject.AddComponent<NavMeshAgent>();
        }

        Camera mainCamera = Camera.main;
        if (mainCamera)
        {
            m_MainCamera = mainCamera;
        }
        else
        {
            Debug.LogError("There's no main camera !!!");
        }
       
        GameObject.DontDestroyOnLoad(gameObject);//Block the destruction of the object between the scenes        
    }
    
    /// <summary>
    /// Called each time a level was loaded(not the first one)
    /// Instanciate variable that needs to be reseted each map
    /// </summary>
    private void OnLevelWasLoaded()
    {
        Camera mainCamera = Camera.main;
        if (mainCamera)
        {
            m_MainCamera = mainCamera;
        }
        else
        {
            Debug.LogError("There's no main camera !!!");
        }

        //lose movement point     
        OnChangedRoom();
    }

    /// <summary>
    /// Init the player's members in function of the GameData
    /// </summary>
    /// <param name="_GameData"></param>
    public void Init(GameData _GameData)
    {
        m_MovementPoints = _GameData.StartingMovementPoint;
    }

	#endregion

	
	#region Actions

    private void Update()
    {
        MoveToLastAimedInteractableElement();
        if (m_NavMeshAgent.remainingDistance == 0)
        {
            m_AnimSystem.Play("Idle");
        }
    }

    /// <summary>
    /// Move to the last aimed Intaractable element if there's one
    /// </summary>
    private void MoveToLastAimedInteractableElement()
    {
        if(m_LastAimedInteractableElement!=null)
        {
            GoToDestination(m_LastAimedInteractableElement.transform.position);
        }
    }

    /// <summary>
    /// Find the object at the clicked position and try to activate it if in range, or move to destination if not in range and activate it when arrived to destination
    /// If the object activable need something in the inventory, try to find it and activate it
    /// </summary>    
    /// <param name="_ClickedScreenPosition">The position on the screen clicked</param>
    /// <returns></returns>
    public void DoActionAtScreenPosition(Vector2 _ClickedScreenPosition)
    {
        if(m_MainCamera!=null)
        {
            Ray ray = m_MainCamera.ScreenPointToRay(_ClickedScreenPosition);

            RaycastHit hit;      
    
            if(Physics.Raycast(ray, out hit))
            {
                if(hit.collider!=null)
                {
                    InteractableElement interactable = hit.collider.gameObject.GetComponentInChildren<InteractableElement>();
                    if (interactable != null)
                    {
                        m_LastAimedInteractableElement = interactable;//the interaction will be done when colliding with the InteractablElement                        
                        return;
                    }
                    m_LastAimedInteractableElement = null;
                }
                GoToDestination(hit.point);
            }            
        }       
    }

    /// <summary>
    /// Move the player on the navMesh to the destination
    /// </summary>
    /// <returns></returns>
    public bool GoToDestination(Vector3 _Destination)
    {
        if(m_NavMeshAgent!=null)
        {
            m_AnimSystem.Play("Move");
            m_NavMeshAgent.SetDestination(_Destination);
            return true;
        }
        return false;
    }

	#endregion

	
	#region Events

    /// <summary>
    /// Check if we hitted an interactable element
    /// </summary>
    /// <param name="_Collider"></param>
    private void OnTriggerEnter(Collider _Collider)
    {
        if(_Collider!=null)
        {
            InteractableElement interactableElement = _Collider.gameObject.GetComponentInChildren<InteractableElement>();
            OnInteractableElementCollided(interactableElement);
        }
    }

    /// <summary>
    /// Check if we hitted an interactable element
    /// </summary>
    /// <param name="_Collider"></param>
    private void OnTriggerStay(Collider _Collider)
    {
        if (_Collider != null)
        {
            InteractableElement interactableElement = _Collider.gameObject.GetComponentInChildren<InteractableElement>();
            OnInteractableElementCollided(interactableElement);
        }
    }

    /// <summary>
    /// Check if the element is the last element we wanted to interact with, and interact with it
    /// </summary>
    /// <param name="_InteractableElement"></param>
    private void OnInteractableElementCollided(InteractableElement _InteractableElement)
    {
        if (_InteractableElement != null && _InteractableElement == m_LastAimedInteractableElement)
        {
            _InteractableElement.OnInteract();
            m_LastAimedInteractableElement = null;
        }
    }

    /// <summary>
    /// Called when the inventory was opened by the GUI. Pause players movement
    /// </summary>
    public void OnInventoryOpen()
    {
        if (m_NavMeshAgent != null)
        {
            m_NavMeshAgent.Stop();
        }
    }

    /// <summary>
    /// Called when the inventory was closed by the GUI. UnPause players movement 
    /// </summary>
    public void OnInventoryClosed()
    {
        if (m_NavMeshAgent != null)
        {
            m_NavMeshAgent.Resume();
        }
    }

    /// <summary>
    /// Called when the player changed room, decrease it's movement points
    /// </summary>
    private void OnChangedRoom()
    {
        if (GameManager.Instance != null && GameManager.Instance.GameData!=null)
        {
            m_MovementPoints -= GameManager.Instance.GameData.MovementLostByRoomChange;
        }

        MeshesDegradationController mesh = GameObject.FindObjectOfType<MeshesDegradationController>();
        if (mesh != null)
        {
            Debug.Log("Rust update");
            float rustLevel = ((m_MovementPoints / (float)GameManager.Instance.GameData.StartingMovementPoint));
            mesh.m_rustLevel = rustLevel;
        }

        if(m_MovementPoints<=0)
        {
            OnDeath();
        }
    }

    /// <summary>
    /// Called when the player as no remaining movement points
    /// </summary>
    private void OnDeath()
    {
        if(GameManager.Instance!=null)
        {
            GameManager.Instance.OnEndGame();
        }
    }

	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors
	#endregion

}