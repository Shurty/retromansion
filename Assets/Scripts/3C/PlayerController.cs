﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[AddComponentMenu("Scripts/PlayerController")]
	[RequireComponent(typeof(Player))]

#endregion

///<summary>
/// 
///</summary>
public class PlayerController : MonoBehaviour
{

	#region Members

    Camera m_MainCamera;
    Player m_Player;

	#endregion

	
	#region Initialisation / Destruction

    private void Start()
    {
        Camera mainCamera = Camera.main;
        if (mainCamera)
        {
            m_MainCamera = mainCamera;
        }
        else
        {
            Debug.LogError("There's no main camera !!!");
        }

        Player player=GetComponent<Player>();
        if(player!=null)
        {
            m_Player = player;
        }
    }

	#endregion

	
	#region Actions

    private void Update()
    {
        if(!CheckTouchScreenInput())
        {
            CheckSelectedPositionInput();
        }       
    }

    /// <summary>
    /// Check if one touch scren input was made, on mobile
    /// React to the input.Touch for the mobile
    /// </summary>
    private bool CheckTouchScreenInput()
    {
        if(Input.touchCount==1 && m_MainCamera!=null)
        {           
            Vector3 clikedPosition = m_MainCamera.ScreenToWorldPoint(Input.GetTouch(0).position);
            m_Player.DoActionAtScreenPosition(clikedPosition);              
        }           
        return false;
    }

    /// <summary>
    /// Check if the player selected a position to move to or to interact with
    /// React when not on mobile, to mouse click or other input defined in editor
    /// </summary>
    private void CheckSelectedPositionInput()
    {
        if (Input.GetButtonDown("PositionSelected"))
        {
           m_Player.DoActionAtScreenPosition(Input.mousePosition);
        }        
    }

	#endregion

	
	#region Events
	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors
	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}