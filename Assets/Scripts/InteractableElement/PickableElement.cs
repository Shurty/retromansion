﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[AddComponentMenu("Scripts/PickableElement")]
	//[RequireComponent(typeof())]

#endregion

///<summary>
/// 
///</summary>
public class PickableElement : InteractableElement
{

	#region Members

		[Header("Pickable settings")]

		[SerializeField]	private		bool	m_Edible		= true;
		[SerializeField]	private		Sprite	m_UIItemView	= null;

	#endregion

	
	#region Initialisation / Destruction
	#endregion

	
	#region Actions

		/// <summary>
		/// Called when Player want to get this object.
		/// </summary>
		public override bool OnInteract()
		{
			if(!base.OnInteract())
			{
				return false;
			}

			else
			{
				PlayerInventory.Instance.AddInInventory(this);
				gameObject.SetActive(false);
			}

			return true;
		}

	#endregion

	
	#region Events

		public virtual void OnUse()
		{
			Debug.Log("PickableELement.OnUse()");
		}

	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors

		public bool IsEdible
		{
			get { return m_Edible; }
		}

		public Sprite UIItemView
		{
			get { return m_UIItemView; }
		}

	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}