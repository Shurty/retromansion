﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[AddComponentMenu("Scripts/PickableTest")]
	//[RequireComponent(typeof())]

#endregion

///<summary>
/// 
///</summary>
public class PickableTest : PickableElement
{

	#region Members
	#endregion

	
	#region Initialisation / Destruction
	#endregion

	
	#region Actions

		public override void OnUse()
		{
			Debug.Log("Name = " + ItemName + " used !");
		}

	#endregion

	
	#region Events
	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors
	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}