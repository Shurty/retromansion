﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[AddComponentMenu("Scripts/InteractableElement")]
	//[RequireComponent(typeof())]

#endregion

///<summary>
/// 
///</summary>
public abstract class InteractableElement : MonoBehaviour
{

	#region Members

		// Settings

		[Header("Interactable settings")]

		[SerializeField]	private		string			m_ItemName				= string.Empty;

		[Header("Conditions settings")]

		[Tooltip("The minimal quest level to make this object visible.")]
		[SerializeField]	private		int				m_MinQuestLevel			= 1;

		[Tooltip("The maximal quest level to make this object visible.")]
		[SerializeField]	private		int				m_MaxQuestLevel			= 9;

		[Space()]
	
		[Tooltip("While the named elements have not been activated by Player, this object will not be visible.")]
		[SerializeField]	private		List<string>	m_Conditions			= new List<string>();

		[Tooltip("If this option is checked, the object will always be visible, but usable only when Quest Level and conditions are correct.")]
		[SerializeField]	private		bool			m_AlwaysVisible			= false;

	#endregion

	
	#region Initialisation / Destruction

		private void Awake()
		{
			SetActive();
		}

	#endregion

	
	#region Actions
	#endregion

	
	#region Events

		/// <summary>
		/// Called when Player interacts with this object.
		/// </summary>
		public virtual bool OnInteract()
		{
			if(CanInteract())
			{
				PlayerInventory.Instance.NotifyActivated(m_ItemName);
			}

			else
			{
				OnDenyInteraction();
				return false;
			}

			return true;
		}

		protected virtual void OnDenyInteraction()
		{

		}

	#endregion

	
	#region Checkers

		public bool AreConditionsChecked()
		{
			for(int i = 0; i < m_Conditions.Count; i++)
			{
				if(!PlayerInventory.Instance.HasBeenActivated(m_Conditions[i]))
				{
					return false;
				}
			}

			return true;
		}

		protected virtual bool CanInteract()
		{
			return (AreConditionsChecked() && IsInQuestLevelRange());
		}

		private bool IsInQuestLevelRange()
		{
			int questLevel = GameManager.Instance.QuestLevel;

			return (questLevel >= m_MinQuestLevel && questLevel <= m_MaxQuestLevel);
		}

	#endregion

	
	#region Accessors

		private void SetActive()
		{
			bool isActive = false;

			if(!PlayerInventory.Instance.IsInInventory(m_ItemName))
			{
				if(m_AlwaysVisible || (IsInQuestLevelRange() && AreConditionsChecked()))
				{
					isActive = true;
				}
			}

			gameObject.SetActive(isActive);
		}

		public string ItemName
		{
			get { return m_ItemName; }
		}

	#endregion

}