﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	[AddComponentMenu("Scripts/Interactable Elements/Door")]
	//[RequireComponent(typeof())]

#endregion

///<summary>
/// 
///</summary>
public class Door : InteractableElement
{

	#region Members

		// References

							private		AudioSource		m_AudioSource	= null;

		// Settings

		[Header("Settings")]

		[SerializeField]	private		string			m_Unlocker		= string.Empty;
		[SerializeField]	private		string			m_LevelToLoad	= string.Empty;
        [SerializeField]    private     DirectionEnum   m_Direction     = DirectionEnum.Count;

	#endregion

	
	#region Initialisation / Destruction

		private void Awake()
		{
			m_AudioSource = GetComponent<AudioSource>();
		}

	#endregion

	
	#region Actions
	#endregion

	
	#region Events

		/// <summary>
		/// If Player needs an object to open the door, the door will open only if the named object is in its inventory.
		/// Else, the door will open.
		/// </summary>
		public override bool OnInteract()
		{
			if (!base.OnInteract())
			{
				return false;
			}

			else
			{
				Application.LoadLevel(m_LevelToLoad);
                
                if(SpawnManager.Instance!=null)
                {
                    SpawnManager.Instance.OnExitLevel(this);
                }

				return true;
			}
		}

		protected override void OnDenyInteraction()
		{
			base.OnDenyInteraction();

			m_AudioSource.Play();
		}

	#endregion

	
	#region Checkers

		protected override bool CanInteract()
		{
			if(!base.CanInteract())
			{
				return false;
			}

			else
			{
				if(m_Unlocker != string.Empty)
				{
					PickableElement unlocker = PlayerInventory.Instance.GetElementByName(m_Unlocker);

					if (unlocker != null && unlocker.GetComponent<Clue>() != null)
					{
						Debug.LogWarning("You can't use a Clue as an Unlocker for a door.");

						return false;
					}

					return PlayerInventory.Instance.UseElement(m_Unlocker);
				}

				return true;
			}
		}

	#endregion

	
	#region Accessors

		public DirectionEnum Direction
		{
		    get { return m_Direction; }
		}

	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}