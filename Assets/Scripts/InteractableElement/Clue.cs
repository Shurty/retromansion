﻿

#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	[AddComponentMenu("Scripts/Interactable Elements/Clue")]
	//[RequireComponent(typeof())]

#endregion

///<summary>
/// 
///</summary>
public class Clue : PickableElement
{

	#region Members

		// References

		[Header("References")]

		[SerializeField]	private		Sprite				m_CluePositiveImage		= null;
		[SerializeField]	private		Sprite				m_ClueNegativeImage		= null;

		// Settings

		[Header("Clue settings")]

		[SerializeField]	private		int					m_ClueIndex				= 1;
		[SerializeField]	private		int					m_NbStepsMaxToBeFound	= 3;

		// Flow

		[HideInInspector]
		[SerializeField]	private		Sprite				m_ClueImage				= null;

	#endregion

	
	#region Initialisation / Destruction

		private void Start()
		{
			if(GameManager.Instance.QuestLevel == m_ClueIndex)
			{
				gameObject.SetActive(true);
			}

			else
			{
				gameObject.SetActive(false);
			}
		}

	#endregion

	
	#region Actions
	#endregion

	
	#region Events

		public override bool OnInteract()
		{
			if(!base.OnInteract())
			{
				return false;
			}

			else
			{
				SetImage();
				GameManager.Instance.OnPickClue();

                UIPlayerInventory.Instance.OpenClue(this);

				return true;
			}
		}

		/// <summary>
		/// Called when Player want to open it in game.
		/// </summary>
		public override void OnUse()
		{
            UIPlayerInventory.Instance.OpenClue(this);
		}

	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors

		/// <summary>
		/// If NbStepsMaxToBeFound is equal to 0, ClueImage will be selected randomly.
		/// Else, if GameManager.NbStepsSinceLastClue is less or equal to NbStepsMaxToBeFound, ClueImage will be positive... Negative if not.
		/// </summary>
		private void SetImage()
		{
			if(m_NbStepsMaxToBeFound == 0)
			{
				m_ClueImage = GetRandomImage();
			}

			else
			{
				m_ClueImage = (GameManager.Instance.NbStepsSinceLastClue <= m_NbStepsMaxToBeFound) ? m_CluePositiveImage : m_ClueNegativeImage;
			}
		}

		/// <summary>
		/// Get a random image between CluePositiveImage and ClueNegativeImage.
		/// </summary>
		private Sprite GetRandomImage()
		{
			int idx = Random.Range(0, 2);

			return (idx == 0) ? m_CluePositiveImage : m_ClueNegativeImage;
		}

		public Sprite ClueImage
		{
			get { return m_ClueImage; }
		}

	#endregion

}