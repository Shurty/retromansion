﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[AddComponentMenu("Scripts/LightSensibleElement")]
	[RequireComponent(typeof(InteractableElement))]

#endregion

///<summary>
/// 
///</summary>
public class LightSensibleElement : MonoBehaviour
{

	#region Members
	#endregion

	
	#region Initialisation / Destruction

    private void Awake()
    {
        Hide();
    }

	#endregion

	
	#region Actions

    /// <summary>
    /// Make the gameObject visible for the player
    /// called each frame under light
    /// </summary>
    protected virtual void Show()
    {
        MeshRenderer m = GetComponent<MeshRenderer>();
        if(m!=null)
        {
            m.enabled = true;
        }

        InteractableElement interactor = GetComponentInChildren<InteractableElement>();
        if (interactor)
        {
            interactor.enabled = true;
        }
    }

    /// <summary>
    /// Make the gameObject unvisible for the player
    /// called each frame under darkness
    /// </summary>
    protected virtual void Hide()
    {
        MeshRenderer m = GetComponent<MeshRenderer>();
        if (m != null)
        {
            m.enabled = false;
        }

        InteractableElement interactor = GetComponentInChildren<InteractableElement>();
        if(interactor)
        {
            interactor.enabled = false;
        }
    }


    private void Update()
    {
        CheckLightReception();
    }

    /// <summary>
    /// Check if the GameObject is receiving any light. Do a simple raycast from every light in the scene and check if the object is at range from the hiting point
    /// Doesn't handle shadows
    /// </summary>
    private void CheckLightReception()
    {
        Light[] allLights = GameObject.FindObjectsOfType<Light>();
        for (int i = 0; i < allLights.Length; i++)
        {
            if(CheckUnderLight(allLights[i]))
            {
                OnLight();
                return;
            }
        }
        OnDarkness();
    }

    /// <summary>
    /// Return true if the gameObject is lighted by the light in param
    /// </summary>
    /// <param name="_Light"></param>
    /// <returns></returns>
    private bool CheckUnderLight(Light _Light)
    {
        switch (_Light.type)
        {
            case LightType.Area:
                return false;//la flemme de gerer les area
            case LightType.Directional:
                return true;
            case LightType.Point:
                return Vector3.Distance(transform.position,_Light.transform.position)<=_Light.range;
            case LightType.Spot:
                return CheckUnderSpotLight(_Light);
        }
        return false;
    }


    Vector3 m_GroundPosition;

    /// <summary>
    /// Return true if the game object is at range from the light using pythagore 
    /// only working with spotlights
    /// </summary>
    /// <param name="_Light"></param>
    /// <returns></returns>
    private bool CheckUnderSpotLight(Light _Light)
    {
        if(_Light.type==LightType.Spot)
        {
            //Vector3 rangeAtPosition=
            float cosA  = Mathf.Cos(_Light.spotAngle);
            float sinA  = Mathf.Sin(_Light.spotAngle);

            Vector3 spotLightToPosition = transform.position - _Light.transform.position;
            float magn = spotLightToPosition.magnitude;

            float range = magn - (cosA * cosA);
            float dist = magn - (sinA * sinA);

           

            Vector3 groundPosition = _Light.transform.position+(_Light.transform.forward*dist);
            m_GroundPosition = groundPosition;
            return Vector3.Distance(groundPosition, transform.position) <= range * 0.5f;
        }
        return false;
    }

	#endregion

	
	#region Events

    /// <summary>
    /// Called when the element received light from one or more light of the scene
    /// called each frame under light
    /// </summary>
    private void OnLight()
    {    
        Show();
    }

    /// <summary>
    /// Called when the element is not lighted by any light on the scene
    /// called each frame under darkness
    /// </summary>
    private void OnDarkness()
    {
      
    }

	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors
	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(m_GroundPosition,0.3f);
        }
        
		#endif

	#endregion

}