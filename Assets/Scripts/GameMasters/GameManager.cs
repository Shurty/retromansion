﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[AddComponentMenu("Scripts/GameManager")]
	//[RequireComponent(typeof())]

#endregion

///<summary>
/// The Game manager contains the victory/lose conditions, spawn the player etc
///</summary>
public class GameManager : MonoSingleton<GameManager>
{

	#region Members

    [SerializeField]
    private GameData    m_GameData;

    [Tooltip("The player spawn the first time")]
    [SerializeField]
    private Player      m_PlayerPrefab;
    private Player      m_PlayerInstantiated;

	private	int			m_QuestLevel				= 1;
	private	int			m_NbStepsSinceLastClue		= 0;

    private bool m_EndGame = false;

	#endregion

	
	#region Initialisation / Destruction

    /// <summary>
    /// Instantiate a player on the scene if not already on it
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
        m_PlayerInstantiated = GameObject.FindObjectOfType<Player>();
        if(m_PlayerInstantiated==null)
        {
            if (m_PlayerPrefab != null)
            {
                m_PlayerInstantiated = GameObject.Instantiate(m_PlayerPrefab);
                m_PlayerInstantiated.Init(m_GameData);
                m_PlayerInstantiated.transform.position = new Vector3(0, 1, 0);
            }         
        }
    }

	#endregion

	
	#region Actions
	#endregion

	
	#region Events

    /// <summary>
    /// Called when the player is dead
    /// </summary>
    public void OnEndGame()
    {
        if (m_EndGame)
            return;
        m_EndGame = true;
        Destroy(m_PlayerInstantiated.gameObject);
        Application.LoadLevel("Defeat");
        Debug.Log("End Game");
    }

	public void OnWinGame()
	{
        if (m_EndGame)
            return;
        m_EndGame = true;
        Destroy(m_PlayerInstantiated.gameObject);
        Application.LoadLevel("Victory");
        Debug.Log("Win Game");
    }

	private void OnLevelWasLoaded()
	{
		m_NbStepsSinceLastClue++;
	}

	public void OnPickClue()
	{
		PlayerInventory.Instance.ClearActivatedElements();
		m_QuestLevel++;
		m_NbStepsSinceLastClue = 0;
	}

	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors

	public Player PlayerInstance
	{
		get { return m_PlayerInstantiated; }
	}

    public GameData GameData
    {
        get { return m_GameData; }
	}

	public int QuestLevel
	{
		get { return m_QuestLevel; }
	}

	public int NbStepsSinceLastClue
	{
		get { return m_NbStepsSinceLastClue; }
	}

	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR

        [SerializeField]
        private string m_DebugSceneToLoad="";

        /// <summary>
        /// Go to the scene set in m_DebugSceneToLoad, or reload the actual scene
        /// </summary>
        [ContextMenu("ChangeScene")]
        public void ChangeScene()
        {
            if(m_DebugSceneToLoad!="")
            {
                Application.LoadLevel(m_DebugSceneToLoad);
            }
            else
            {
                Application.LoadLevel(Application.loadedLevel);
            }
        }

		#endif

	#endregion

}