﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[AddComponentMenu("Scripts/SpawnManager")]
	//[RequireComponent(typeof())]

#endregion

///<summary>
/// Used to handle were the player Spawn on a level
///</summary>
public class SpawnManager : MonoSingleton<SpawnManager>
{

	#region Members

    [SerializeField]
    private float   m_DistanceFromDoorToSpawnPlayer=4;

    private DirectionCombinationStruct[] m_AllPossibleDirections = new DirectionCombinationStruct[4]
    {
        new DirectionCombinationStruct(DirectionEnum.Left,DirectionEnum.Right),
        new DirectionCombinationStruct(DirectionEnum.Right,DirectionEnum.Left),
        new DirectionCombinationStruct(DirectionEnum.Top,DirectionEnum.Down),
        new DirectionCombinationStruct(DirectionEnum.Down,DirectionEnum.Top) 
    };

    private List<ExitPosition> m_ExitPositions = new List<ExitPosition>();

    private ExitPosition       m_PreviousExitPosition;

	#endregion

	
	#region Initialisation / Destruction

    protected override void Awake()
    {
		base.Awake();

        m_ExitPositions = FindAllExitPositionOnLevel();
    }

    /// <summary>
    /// Find all the doors on the level and fill the exitPosition list
    /// </summary>
    private void OnLevelWasLoaded()
    {
        Debug.Log("Level was loaded");
        m_ExitPositions.Clear();
        m_ExitPositions = FindAllExitPositionOnLevel();
        SetPlayerPosition();
    }

	#endregion

	
	#region Actions

    private void SetPlayerPosition()
    {
        Player player = GameObject.FindObjectOfType<Player>();
        if(player!=null && m_PreviousExitPosition!=null)
        {
            ExitPosition doorToSpawnTo = FindOppositeExitPosition(m_PreviousExitPosition);
            if(doorToSpawnTo!=null && doorToSpawnTo.m_Exit!=null)
            {
                player.transform.position = doorToSpawnTo.m_Exit.transform.position + doorToSpawnTo.m_Exit.transform.forward * m_DistanceFromDoorToSpawnPlayer;
            }
        }
    }
    
    private ExitPosition FindOppositeExitPosition(ExitPosition _ExitPosition)
    {
        ExitPosition mostPossiblePosition = null;
        if(_ExitPosition!=null)
        {
            
            for (int i = 0; i < m_ExitPositions.Count; i++)
            {
                Debug.Log("Direction ="+_ExitPosition.m_DirectionCombinaison.m_Direction+"OppositeDirection ="+_ExitPosition.m_DirectionCombinaison.m_OppositeDirection);
                if(m_ExitPositions[i]!=null && m_ExitPositions[i].m_DirectionCombinaison.m_Direction==_ExitPosition.m_DirectionCombinaison.m_OppositeDirection)
                {
                    if(m_ExitPositions[i].m_Index==_ExitPosition.m_Index)
                    {
                        return m_ExitPositions[i];
                    }       
                    else
                    {
                        mostPossiblePosition = m_ExitPositions[i];
                    }
                }
            }
        }
        return mostPossiblePosition;
    }

    private List<ExitPosition> FindAllExitPositionOnLevel()
    {
        
        Door[] doorsOnLevel = GameObject.FindObjectsOfType<Door>();
        List<ExitPosition> exitPositions = new List<ExitPosition>();
        for (int i = 0; i < doorsOnLevel.Length;i++)
        {
            exitPositions.Add(new ExitPosition(doorsOnLevel[i], FindDirectionCombinaison(doorsOnLevel[i].Direction), FindHighestExitPositionIndexOfDirection(exitPositions, doorsOnLevel[i].Direction)));
        }
        Debug.Log("FindAllExitPositionOnLevel!!  exitPositions.count=" + exitPositions.Count);
        return exitPositions;
    }
    
    private DirectionCombinationStruct FindDirectionCombinaison(DirectionEnum _Direction)
    {
        if(_Direction!=DirectionEnum.Count)
        {
            for (int i = 0; i < m_AllPossibleDirections.Length; i++)
            {
                if(m_AllPossibleDirections[i].m_Direction==_Direction)
                {
                    return m_AllPossibleDirections[i];
                }
            }
        }
        return new DirectionCombinationStruct(DirectionEnum.Count,DirectionEnum.Count);
    }

    private int FindHighestExitPositionIndexOfDirection(List<ExitPosition> _ExitPositions,DirectionEnum _Direction)
    {
        int highestIndex=0;
        for(int i=0;i<_ExitPositions.Count;i++)
        {
            if(_ExitPositions[i].m_Exit.Direction==_Direction)
            {
                if(_ExitPositions[i].m_Index>highestIndex)
                {
                    highestIndex = _ExitPositions[i].m_Index;
                }
            }
        }
        return highestIndex;
    }

    private DirectionEnum FindOppositeDirection(DirectionEnum _Direction)
    {
        for (int i = 0; i < m_AllPossibleDirections.Length; i++)
		{
			 if(m_AllPossibleDirections[i].m_Direction==_Direction)
             {
                 return m_AllPossibleDirections[i].m_OppositeDirection;
             }
		}
        return DirectionEnum.Count;
    }

	#endregion

	
	#region Events

    public void OnExitLevel(Door _ExitingDoor)
    {
        for (int i = 0; i < m_ExitPositions.Count; i++)
        {
            if(m_ExitPositions[i].m_Exit==_ExitingDoor)
            {
                m_PreviousExitPosition = m_ExitPositions[i];
            }
        }
    }
	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors
	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}

public class ExitPosition
{
    public Door                         m_Exit;//The door or stairs 
    public DirectionCombinationStruct   m_DirectionCombinaison;
    public int                          m_Index;//The index count the position of the door on the direction. for example a door that is on the left of the map, and that is the 3 one when starting from the bottom as a 3 index

    public ExitPosition(Door _Door, DirectionCombinationStruct _Combination,int _Index)
    {
        if(_Door.Direction==DirectionEnum.Count)
        {
            Debug.LogError("The door " + _Door.name + " as not a correct Direction !!!");
        }
        m_Exit = _Door;
        m_DirectionCombinaison = _Combination;
        m_Index = _Index;
    }
}

public struct DirectionCombinationStruct
{
    public DirectionEnum m_Direction;
    public DirectionEnum m_OppositeDirection;

    public DirectionCombinationStruct(DirectionEnum _Direction,DirectionEnum _OppositeDirection)
    {
        m_Direction = _Direction;
        m_OppositeDirection = _OppositeDirection;
    }
}

public enum DirectionEnum { Left, Right, Top, Down, Count}
