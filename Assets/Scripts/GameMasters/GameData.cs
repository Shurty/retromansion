﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[System.Serializable]

#endregion

///<summary>
/// 
///</summary>
[System.Serializable]
public class GameData
{

	#region Members

    [Tooltip("The count of movement the player can do before losing the game")]
    [SerializeField]
    private int m_PlayerStartingMovementCount=100;

    [Tooltip("The count of movements the player loose each time he changes room")]
    [SerializeField]
    private int m_MovementLostByRoomChange=1;

	#endregion

	
	#region Initialisation / Destruction

		/*public GameData()
		{
		
		}*/

	#endregion

	
	#region Actions
	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors

    public int StartingMovementPoint
    {
        get { return m_PlayerStartingMovementCount; }
    }

    public int MovementLostByRoomChange
    {
        get { return m_MovementLostByRoomChange; }
    }
	#endregion

}