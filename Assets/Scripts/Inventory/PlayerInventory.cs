﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[System.Serializable]

#endregion

///<summary>
/// 
///</summary>
public class PlayerInventory : Singleton<PlayerInventory>
{

	#region Members

		private		List<PickableElement>	m_PickableElements	= new List<PickableElement>();
		private		List<string>			m_ActivatedElements	= new List<string>();

	#endregion

	
	#region Initialisation / Destruction

		private PlayerInventory()
		{
			
		}

	#endregion

	
	#region Actions

		/// <summary>
		/// Get an element with the given name in the list.
		/// If one is found, call its function OnUse() and remove it from the list.
		/// </summary>
		/// <returns>Returns true if an element was found in the list and used, false if not.</returns>
		public bool UseElement(string _ElementName)
		{
			for(int i = 0; i < m_PickableElements.Count; i++)
			{
				if(m_PickableElements[i].ItemName == _ElementName)
				{
					m_PickableElements[i].OnUse();

					if(m_PickableElements[i].IsEdible)
					{
						m_PickableElements.RemoveAt(i);
					}

					return true;
				}
			}

			return false;
		}

		public void NotifyActivated(string _ActivatedElementName)
		{
			if(!HasBeenActivated(_ActivatedElementName))
			{
				m_ActivatedElements.Add(_ActivatedElementName);
			}
		}

		public void ClearActivatedElements()
		{
			m_ActivatedElements.Clear();
		}

	#endregion

	
	#region Checkers

		public bool HasBeenActivated(string _ElementName)
		{
			for(int i = 0; i < m_ActivatedElements.Count; i++)
			{
				if(_ElementName == m_ActivatedElements[i])
				{
					return true;
				}
			}

			return false;
		}

		public bool IsInInventory(string _ElementName)
		{
			for(int i = 0; i < m_PickableElements.Count; i++)
			{
				if(m_PickableElements[i].ItemName == _ElementName)
				{
					return true;
				}
			}

			return false;
		}

	#endregion

	
	#region Accessors#region Accessors

		/// <summary>
		/// Check if the given InteractableElement is of type PickableElement.
		/// If true, clone the original script and add it to the list as a PickableElement.
		/// </summary>
		public void AddInInventory(PickableElement _Element)
		{
			m_PickableElements.Add(_Element);
		}

		public PickableElement GetElementByName(string _ElementName)
		{
			for(int i = 0; i < m_PickableElements.Count; i++)
			{
				if(m_PickableElements[i].ItemName == _ElementName)
				{
					return m_PickableElements[i];
				}
			}

			return null;
		}

		public List<PickableElement> PickableElements
		{
			get { return m_PickableElements; }
		}

	#endregion

}