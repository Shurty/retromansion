﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct PPColorProgressKey
{
    public string m_name;
    public Vector3 m_intensity;
    public Vector3 m_tone;
    public float m_changeAtValue;
    public bool m_renderShader;
}
