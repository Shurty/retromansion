﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShaderControllerComponent : MonoBehaviour {

    public List<MeshRenderer> m_targetMeshes;

    // <summary>
    //    -
    // </summary>
	void Start () {
        m_targetMeshes = new List<MeshRenderer>();
	}

    // <summary>
    //    -
    // </summary>
	void Update () {
	
	}

    // <summary>
    //    Set all the related meshs shader values 
    // </summary>
    public void SetToAll(string key, float value)
    {
        foreach (MeshRenderer mesh in m_targetMeshes)
        {
            mesh.material.SetFloat(key, value);
        }
    }
}
