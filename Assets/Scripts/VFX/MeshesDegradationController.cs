﻿using UnityEngine;
using System.Collections;

public class MeshesDegradationController : ShaderControllerComponent {

    [Range(0, 1)]
    public float m_rustLevel = 0.0f;

    // <summary>
    //    -
    // </summary>
	void Start () {
	
	}

    // <summary>
    //    Apply the degradation to all related meshes
    // </summary>
	void Update () {
        SetToAll("_Degradation", m_rustLevel);
	}

}
