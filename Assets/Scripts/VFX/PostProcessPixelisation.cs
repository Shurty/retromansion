﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PostProcessPixelisation : UnityStandardAssets.ImageEffects.ImageEffectBase
{
    [Range(0, 1)]
    public float m_pixelisationLevel = 1.0f;
    
    // <summary>
    //    Called by external components when the color value has changed
    //    Modifiy the current Post processing overlay color
    // </summary>
    public void ProcessValueChanged(float newValue)
    {
        m_pixelisationLevel = newValue;
        SetShaderMaterial();
    }

    // <summary>
    //    Get and set the active color post-process into the render shader
    // </summary>
    public void SetShaderMaterial()
    {
        material.SetFloat("_Intensity", (int)(m_pixelisationLevel * 100.0f));
    }

    // <summary>
    //    Called when the final image is rendered and sets the related shader
    // </summary>
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        SetShaderMaterial();
        Graphics.Blit(source, destination, material);
    }

}
