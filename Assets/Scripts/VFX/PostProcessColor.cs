﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PostProcessColor : UnityStandardAssets.ImageEffects.ImageEffectBase
{
    public float m_colorProgressionValue = 1.0f;
    public PPColorProgressKey[] m_colorProgressionKeys;
    public PPColorProgressKey m_activeColorProg;
    
    // <summary>
    //    Returns the active Post processing color key
    // </summary>
    public PPColorProgressKey GetActiveColorProgression()
    {
        PPColorProgressKey def = m_colorProgressionKeys[0];

        foreach (PPColorProgressKey cpk in m_colorProgressionKeys)
        {
            if (cpk.m_changeAtValue > m_colorProgressionValue)
                return (def);
            def = cpk;
        }
        return (def);
    }

    // <summary>
    //    Returns the active Post processing color tone
    // </summary>
    public Vector3 GetActiveColorProgressionTone()
    {
        PPColorProgressKey progColor = GetActiveColorProgression();
        return (progColor.m_tone);
    }

    // <summary>
    //    Returns the active Post processing color intensity
    // </summary>
    public Vector3 GetActiveColorProgressionIntensity()
    {
        PPColorProgressKey progColor = GetActiveColorProgression();
        return (progColor.m_intensity);
    }

    // <summary>
    //    Called by external components when the color value has changed
    //    Modifiy the current Post processing overlay color
    // </summary>
    public void ProcessColorValueChanged(float newValue)
    {
        m_colorProgressionValue = newValue;
        SetShaderMaterial();
    }

    // <summary>
    //    Get and set the active color post-process into the render shader
    // </summary>
    public void SetShaderMaterial()
    {
        m_activeColorProg = GetActiveColorProgression();
        material.SetVector("_Intensity", m_activeColorProg.m_intensity);
        material.SetVector("_ColorConvert", m_activeColorProg.m_tone);
    }

    // <summary>
    //    Called when the final image is rendered and sets the related shader
    // </summary>
    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
#if UNITY_EDITOR
        SetShaderMaterial();
#endif
        if (!m_activeColorProg.m_renderShader)
            return;
        Graphics.Blit(source, destination, material);
    }

}
