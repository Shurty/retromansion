﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class AutoRescale : MonoBehaviour {

    public float m_absDistance = 1.00f;
    public float m_absRatio = 1.7778f;

    // <summary>
    //    OnStart rescale of the camera depending of the active resolution
    // </summary>
	void Start () {
        ForceRescaling();
	}

    // <summary>
    //    -
    // </summary>
    void Update () 
    {
#if UNITY_EDITOR
        ForceRescaling();
#endif
	}

    // <summary>
    //    Force the camera rescaling to a correct ortho. size
    // </summary>
    void ForceRescaling()
    {
        float ratioDiff = ((float)Screen.width / (float)Screen.height);
        float resRatio = ratioDiff / m_absRatio;

        GetComponent<Camera>().orthographicSize = (m_absDistance / resRatio);
    }
}
