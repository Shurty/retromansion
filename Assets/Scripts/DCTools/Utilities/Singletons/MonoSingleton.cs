﻿using UnityEngine;

///<summary>
///
///		Use this class to create a MonoBehaviour class with a
///	Singleton behavior.
/// 
///		Note that all the GameObject will be saved as the only
///	instance, not only the current script.
/// 
///</summary>

public class MonoSingleton<T> : MonoBehaviour
	where T : MonoBehaviour
{

	#region Members

		// References

		// Contains the only instance of this class.
							private		static	T					s_Instance				= null;

		// Settings

		[Header("Singleton Settings")]

		[Tooltip("If this option is checked, the saved instance will never be replaced. Else, it will be replaced or deleted at every level load.")]
		[SerializeField]	private				bool				m_DontDestroyOnLoad		= true;

		[Tooltip("If this option is checked, if this singleton has already an instance at its Awake, destroy the entire gameObject instead of the component only.")]
		[SerializeField]	private				bool				m_DestroyGameObject		= false;

	#endregion


	#region Initialisation / Destruction

		protected virtual void Awake()
		{
			SetInstance();

			if(m_DontDestroyOnLoad)
			{
				DontDestroyOnLoad(this);
			}
		}

	#endregion


	#region Actions

		private static void NotifyInstanceCreation()
		{
			if(s_Instance != null)
			{
				(s_Instance as MonoSingleton<T>).OnCreateInstance();
			}
		}

	#endregion


	#region Events

		/// <summary>
		/// This event is called when the Instance has successfully been set.
		/// You must init an object with this event. In fact, the instance
		/// can be set before the Awake() of this MonoSingleton, so an
		/// operation could not been done when it's required.
		/// </summary>
		public virtual void OnCreateInstance()
		{
			
		}

	#endregion


	#region Accessors

		/// <summary>
		/// If there's no instance of this class, the only instance become
		/// this one. Else, this component is destroyed.
		/// </summary>
		private void SetInstance()
		{
			if (s_Instance == null)
			{
				s_Instance = this as T;

				NotifyInstanceCreation();
			}

			else
			{
				/*
				 * The instance can be initialized by another object, before
				 * the Awake() of this MonoBehaviour. In this case, the
				 * instance will be found with FindObjectOfType<T>().
				 * For this reason, we have to check if the current instance
				 * is different of this object, else we could destroy the
				 * current instance by accident.
				 */
				if(s_Instance != this)
				{
					DestroyInstance();
				}
			}
		}

		/// <summary>
		/// If no instance has been already set, this accessor will create
		/// an empty GameObject, and add to it the component T. The added
		/// component will become the only instance.
		/// </summary>
		/// <returns>Returns the only instance of this class.</returns>
		public static T Instance
		{
			get
			{
				if (s_Instance == null)
				{
					s_Instance = FindObjectOfType<T>();

					NotifyInstanceCreation();
				}

				return s_Instance;
			}
		}

		/// <summary>
		/// If the DestroyGameObject option is checked, destroy the entire
		/// GameObject. Else, destroy only this component.
		/// </summary>
		private void DestroyInstance()
		{
			if(m_DestroyGameObject)
			{
				Destroy(gameObject);
			}

			else
			{
				Destroy(this);
			}
		}

	#endregion

}