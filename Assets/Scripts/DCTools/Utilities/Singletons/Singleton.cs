﻿using System;

/// <summary>
/// 
///		Use this class to create a thread-safe singleton.
/// 
/// </summary>
/// 
/// <typeparam name="T">Type of the singleton class.</typeparam>

public class Singleton<T>
	where T : class
{

	#region Members

		// Resources

		// Contains the only instance of the class.
		protected	static T		s_Instance = null;

		private		static object	s_InitLock = new object();

	#endregion


	#region Initialisation / Destruction

		protected Singleton()
		{

		}

	#endregion


	#region Accessors

		/// <summary>
		/// Create a (thread-safe) instance and save it if it doesn't exist.
		/// </summary>
		/// <returns>Returns the only instance of the class.</returns>
		public static T Instance
		{
			get
			{
				if (s_Instance == null)
				{
					lock (s_InitLock)
					{
						if (s_Instance == null)
						{
							Type t = typeof(T);

							System.Reflection.ConstructorInfo[] constructors = t.GetConstructors();

							if (constructors.Length > 0)
							{
								throw new InvalidOperationException("Error : A class using Singleton must not have any public constructor.");
							}

							s_Instance = (T)Activator.CreateInstance(t, true);
						}
					}
				}

				return s_Instance;
			}
		}

	#endregion

}