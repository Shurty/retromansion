﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[AddComponentMenu("Scripts/PivotableAccelerometer")]
	[RequireComponent(typeof(RoomDynamicsController))]

#endregion

///<summary>
/// A class allowing an object to pivot with the accelerometer
///</summary>
public class PivotableAccelerometer : MonoBehaviour
{

	#region Members

    [Tooltip("The coeff by wich we multiply the accelerometer values. x value multiply x rotation, y value multiply z rotation")]
    [SerializeField]
    [Min(1)]
    private Vector2 m_RotationCoefficients=new Vector2(1,1);

    [Tooltip("The max Accelerometer values by wich we rotate. x to right axis, y to forward axis ")]
    [SerializeField]
    private Vector2 m_MaxAccelerometerCap=new Vector2(90,90);

    private Vector2 m_ActualAccelerometerValue = new Vector2(0, 0);

    private RoomDynamicsController m_RoomDynamicComponent;

	#endregion

	
	#region Initialisation / Destruction

    private void Awake()
    {
        m_RoomDynamicComponent = gameObject.GetComponentInChildren<RoomDynamicsController>();
    }

	#endregion

	
	#region Actions

    private void Update()
    {
        UpdateActualAccelerometerValue();
        UpdateRotation();
    }

    private void UpdateActualAccelerometerValue()
    {
        m_ActualAccelerometerValue += new Vector2(Input.acceleration.x * m_RotationCoefficients.x, Input.acceleration.y * m_RotationCoefficients.y);
        m_ActualAccelerometerValue = new Vector2(Mathf.Clamp(m_ActualAccelerometerValue.x, -m_MaxAccelerometerCap.x, m_MaxAccelerometerCap.x), Mathf.Clamp(m_ActualAccelerometerValue.y, -m_MaxAccelerometerCap.y, m_MaxAccelerometerCap.y));
    }

    /// <summary>
    /// Update this gameobject's rotation with the RoomDynamicsController function
    /// </summary>
    private void UpdateRotation()
    {
        float xRotation = FindRatio(m_ActualAccelerometerValue.x, m_MaxAccelerometerCap.x);
        float zRotation = FindRatio(m_ActualAccelerometerValue.y, m_MaxAccelerometerCap.y);

        Vector3 wantedRotation = new Vector3(xRotation,0,zRotation);
        m_RoomDynamicComponent.m_activeWindVelocity = wantedRotation;
    }

    /// <summary>
    /// Return the Acceleration ratio in function of the actual accelerometer value and the Cap of accelerometer fixed
    /// </summary>
    /// <param name="_ActualAccelerometerValue"></param>
    /// <param name="_AccelerometerCap"></param>
    /// <returns></returns>
    private float FindRatio(float _ActualAccelerometerValue,float _AccelerometerCap)
    {     
      float percentage = Mathf.Abs(_ActualAccelerometerValue) * 100 / Mathf.Abs(_AccelerometerCap);
      return Mathf.Clamp((Mathf.Abs(percentage) / 100) * Mathf.Sign(_ActualAccelerometerValue),-1,1);
    }

	#endregion

	
	#region Events
	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors
	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}