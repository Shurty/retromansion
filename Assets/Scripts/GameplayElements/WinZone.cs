﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[AddComponentMenu("Scripts/WinZone")]
	[RequireComponent(typeof(Collider))]

#endregion

///<summary>
/// 
///</summary>
public class WinZone : MonoBehaviour
{

	#region Members

    [Tooltip("The minimum number of element in the player's inventory he needs to have when triggering the win zone to win")]
    [SerializeField]
    private int m_MinInventoryElementCountToWin = 10;

	#endregion

	
	#region Initialisation / Destruction
	#endregion

	
	#region Actions
	#endregion

	
	#region Events

    private void OnTriggerEnter(Collider _Collider)
    {
        Player player = _Collider.GetComponentInChildren<Player>();
        if(player!=null)
        {
            if(PlayerInventory.Instance.PickableElements.Count>=m_MinInventoryElementCountToWin)
            {
                if(GameManager.Instance!=null)
                {
                    GameManager.Instance.OnWinGame();
                }
            }
        }
    }

	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors
	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR
		#endif

	#endregion

}