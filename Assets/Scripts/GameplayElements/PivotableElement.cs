﻿#region System headers

	//using System;
	//using System.IO;

	//using System.Collections;
	//using System.Collections.Generic;

	//using System.Reflection;

#endregion


#region Unity headers

	using UnityEngine;

	//using UnityEngine.UI;
	//using UnityEngine.Events;

	//[AddComponentMenu("Scripts/PivotableElement")]
	[RequireComponent(typeof(BoxCollider))]

#endregion

///<summary>
/// 
///</summary>
public class PivotableElement : MonoBehaviour
{

	#region Members

    [Tooltip("The speed at wich the pivotable return to it's original rotation when released by player")]
    [SerializeField]
    private float               m_ReturnToOriginalRotationSpeed;

    [Tooltip("The max angle at wich the pivotable can rotate")]
    [SerializeField]
    private float               m_MaxAngle                                  =   45;

    [Tooltip("The angle at wich we activate the linkedInteractable")]
    [SerializeField]
    private float               m_ActivationAngle                           = 20;

    [Tooltip("If true the pivotable will return to it's original rotation when the player release his finger")]
    [SerializeField]
    private bool                m_ReturnToOriginalRotationWhenReleased      = false;

    [Tooltip("The interactable that will be activated when the pivotable reach the activation angle")]
    [SerializeField]
    private InteractableElement m_LinkedInteractable;

    private Quaternion  m_OriginalRotation;
    private Vector3     m_TouchedPositionOnCollider;
    private Vector3     m_ForwardWhenStartTouched;
    private float       m_ForwardToTouchedPositionAngle;
    private Camera      m_MainCamera;
    private bool        m_Touched                                           =   false;
    private Vector3     m_CursorScreenPosition;
    private Vector3     m_OriginalForward;
    private Vector3     m_BasicComparableVector;

	#endregion

	
	#region Initialisation / Destruction

    private void Awake()
    {
        m_MainCamera = Camera.main;
        m_OriginalRotation = transform.rotation;
        m_OriginalForward = transform.forward;
        m_BasicComparableVector = -transform.right;
        OnDeactivation();
    }

	#endregion

	
	#region Actions

    private void Update()
    {
        if (Input.touchCount == 1 && m_MainCamera != null)
        {        
            CheckCollisionByTouch(Input.GetTouch(0).position);
            RotateToFingerPosition();
        }
        else if (Input.GetButtonDown("PositionSelected"))
        {
            CheckCollisionByTouch(Input.mousePosition);
            RotateToFingerPosition();
        }
        else
        {
            OnReleased();
        }
    }

    /// <summary>
    /// Check if the clicked/touched position is colliding with this, 
    /// </summary>
    private bool CheckCollisionByTouch(Vector2 _ScreenPosition)
    {
        if(m_MainCamera!=null)
        {            
            Ray ray = m_MainCamera.ScreenPointToRay(_ScreenPosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider != null)
                {
                    m_CursorScreenPosition =hit.point;
                    if(hit.collider.gameObject==gameObject)//if the player touched us
                    {                        
                        OnBeginTouch(hit.point);
                        return true;
                    }
                }
            }
        }
        return false;
    }

	#endregion

	
	#region Events

    private void OnBeginTouch(Vector3 _TouchedPosition)
    {        
        if(!m_Touched)
        {
            m_TouchedPositionOnCollider = _TouchedPosition;
            Vector3 pivotToTouchedPosition=_TouchedPosition-transform.position;
            m_ForwardToTouchedPositionAngle = Vector3.Angle(transform.forward, pivotToTouchedPosition.normalized);
            m_Touched = true;
        }        
    }

    /// <summary>
    /// Deactivate the linked interactible
    /// </summary>
    private void OnDeactivation()
    {
        if (m_LinkedInteractable != null)
        {
            m_LinkedInteractable.gameObject.SetActive(false);
        }
    }

    /// <summary>
    /// Activate the linked interactible
    /// </summary>
    private void OnActivation()
    {
        if(m_LinkedInteractable!=null)
        {
            m_LinkedInteractable.gameObject.SetActive(true);
        }
    }

    private void OnReleased()
    {               
        if(m_Touched)
        {
            if(m_ReturnToOriginalRotationWhenReleased)
            {
                transform.rotation = m_OriginalRotation;
            }            
            m_Touched = false;      
        }                
    }

    /// <summary>
    /// Rotate the element so it rotation follow the player's finger position
    /// </summary>
    private void RotateToFingerPosition()
    {        
        if(m_Touched)
        {
            Vector3 pivotToCursorPosition = (m_CursorScreenPosition - transform.position).normalized;
            float angle = Vector3.Angle(pivotToCursorPosition, transform.forward);
            Vector3 forwardRotated = Quaternion.AngleAxis(angle - m_ForwardToTouchedPositionAngle, transform.up) * transform.forward;

            if (Vector3.Dot(forwardRotated,m_BasicComparableVector)>0)//going in the right direction
            {
                float rotationAngle=Vector3.Angle(forwardRotated, m_OriginalForward);
                float actualAngle = Vector3.Angle(transform.forward, m_OriginalForward);
                if (rotationAngle < m_MaxAngle && actualAngle<rotationAngle)//go only one way
                {                
                    transform.LookAt(transform.position + forwardRotated, transform.up);
                }   
 
                if(rotationAngle>=m_ActivationAngle)
                {
                    OnActivation();
                }
            }                    
        }
        //Debug.Break();
    }

	#endregion

	
	#region Checkers
	#endregion

	
	#region Accessors
	#endregion

	
	#region Debug & Tests

		#if UNITY_EDITOR

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(m_CursorScreenPosition, 0.5f);

            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(m_TouchedPositionOnCollider, 0.5f);
        }

		#endif

	#endregion

}